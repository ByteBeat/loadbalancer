using System;
using System.Collections.Generic;
using System.Text;

namespace LoadBalancer.Avalonia.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Branding => "LoadBalancer";
    }
}
